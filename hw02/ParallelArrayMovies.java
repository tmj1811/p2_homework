/**
 * Personal Movie Inventory System.
 *
 * @Tylar Jank
 * @15 January 2020
 *
 * <pre>
 * This is the starter code for the parallel array version of the movie
 * inventory system.
 * </pre>
*/

import java.util.*;
import java.io.*;
public class ParallelArrayMovies
{
	public static final String DATAFILE= "../tinymovielist.txt";
	public static final int MAXMOVIES= 10000;
	public static Scanner kb= new Scanner(System.in);

	public static void main(String [] args)
	{
		int choice;						 // user's selection from the menu
		String [] titles= new String[MAXMOVIES];
		String [] genres= new String[MAXMOVIES];
		int [] years= new int[MAXMOVIES];
		int numberOfEntries;

		numberOfEntries= loadMovies(titles,genres,years);
		System.out.println("Number of entries read from data file: "+numberOfEntries);
		do {
			choice= menuChoice();
			if (choice==1)
				numberOfEntries= enterMovie(titles,genres,years,numberOfEntries);
         else if (choice==2)
            displayAll(titles,genres,years,numberOfEntries);
			else if (choice==3)
				displayAll(titles,genres,years,numberOfEntries);
         else if (choice==4)
            displayAll(titles,genres,years,numberOfEntries);
         else if (choice==5)
            displayAll(titles,genres,years,numberOfEntries);
         else if (choice ==6)
            searchByYear(years);   
		   } 
      
         while (choice!=0);

		System.out.println("\nTHE END");
	}
   
   /**
	 * Saves movie to datafile.
   */
   public static void save(String [] titles, String [] genres, int [] years) {
      /*PrintStream fileOut = new PrintStream(DATAFILE);
      fileOut.println();
      fileOut.close;*/
   }


	/**
	 * Allow user to enter a new movie.
	 *
	 * @param titles array of movie titles
	 * @param genres array of movie genres
	 * @param years array of movie copyright dates
	 * @param n the actual number of movies currently in the array
	 * @return the new movie count
	*/
	public static int enterMovie(String [] titles, String [] genres, int [] years, int n)
	{
		System.out.print("Enter movie title : ");
		titles[n]= kb.nextLine();
		System.out.print("Enter movie genre: ");
		genres[n]= kb.nextLine();
		System.out.print("Enter year of movie: ");
		years[n]= kb.nextInt();
		kb.nextLine();

		return n+1;
	}




	/**
	 * Load movies from the data file into the arrays
	 *
	 * @param titles array of movie titles
	 * @param genres array of movie genres
	 * @param years array of movie copyright dates
	 * @return the actual number of movies loaded into the arrays
	*/
	public static int loadMovies(String [] titles, String [] genres, int [] years)
	{
		int n =0;
      
      try {
         Scanner f = new Scanner(new FileInputStream(DATAFILE));
         while (f.hasNextLine()) {
            titles[n]= f.nextLine();
            genres[n]= f.nextLine();
            years[n]= f.nextInt();
            f.nextLine();
            n++;
         }
      f.close();
   }
   catch(FileNotFoundException e) {
      System.out.println(e);
   }
         
      return n;
	}



	/**
	 * Displays all movie information.
	 *
	 * @param titles array of movie titles
	 * @param genres array of movie genres
	 * @param years array of movie copyright dates
	 * @param n the actual number of movies currently in the array
	*/
	public static void displayAll(String [] titles, String [] genres, int [] years, int n)
	{
		int i;
		System.out.println("------------------------------------------------");
		System.out.printf("%-30s %-20s %s\n","TITLE","GENRE","YEAR");
		for (i=0; i<n; i++)
			System.out.printf("%-30s %-20s %4d\n",titles[i],genres[i],years[i]);
	}


	/**
	 * Displays menu and get's user's selection
	 *
	 * @return the user's menu selection
	*/
	public static int menuChoice()
	{
		Scanner kb= new Scanner(System.in);
		int choice;	 // user's selection

		System.out.println("\n\n");
		System.out.print("------------------------------------\n");
		System.out.print("[1] Add a Movie\n");
		System.out.print("[2] Delete a Movie\n");
		System.out.print("[3] List All Movies\n");
		System.out.print("[4] Search by Genre\n");
		System.out.print("[5] Search by Title\n");
		System.out.print("[6] Search by Year\n");
		System.out.print("---\n");
		System.out.print("[0] Exit Program\n");
		System.out.print("------------------------------------\n");
		do {
			System.out.print("Your choice: ");
			choice= kb.nextInt();
		} while (choice < 0 || choice > 6);

		return choice;
	}
   /**
	 * Allows user to search for movie by the release year.
	 * @param years array of movie copyright dates
	*/

   public static int searchByYear(int[] years){
      Scanner kb = new Scanner(System.in); 
      int year; //integer that user inputs to choose a movie from.
      System.out.println("What year is your movie from? "); 
      year = kb.nextInt();  
      return year;

      }
      /**
	 * Allows user to search for movie by the title.
	 * @param titles array of movie titles
	*/
      
   public static void searchByTitle(String[] titles){
      Scanner kb = new Scanner(System.in);
      String title; //String that user inputs to search for movie title.
      System.out.println("What is the title of your movie? ");
      title = kb.nextLine();
      int pos;
      pos = title.indexOf(title);
      if(title.indexOf(title)>=0){
         System.out.println(pos);
      }
   }
   /**
	 * Allows user to search for movie by the genre.
	 * @param genres array of movie genres
	*/
   public static void searchByGenre(String[] genres){
      Scanner kb = new Scanner(System.in);
      String genre; //String that user inputs to search for movie genre.
      System.out.println("What is the genre of your movie? ");
      genre = kb.nextLine();
      
   }
    /**
	 * Allows user to delete movie from text file.
	 * @param n number of movies in array
    * @return movie list after deleting a movie
	*/
   
   public static int deleteMovie(int n) {
      Scanner kb = new Scanner(System.in);
      int pos; //Integer revealing which movie would like to be deleted.
      
      int a[] = new int [n]; //Integer array containing each movie.
      
      System.out.println("Enter the position of the movie you would like to delete: ");
      pos = kb.nextInt();
      for(int i=pos;i<n-1;i++) {
         a[i]=a[i+1];
      }
      n=n-1;
      return n;
      
     }
   }

   
   

